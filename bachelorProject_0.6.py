# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 17:30:51 2019

@author: Michal
"""
from  scipy import *
#from scipy.misc import derivative
#from scipy.stats import variation
from  pylab import *

from matplotlib import rc

#uncomment the following two lines for LaTeX in plots
#rc('font', **{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)

class eventGenerator:
    def __init__(self, m, mCharge, eventNum, electronArgs, tungArgs, electronDistribution, metalDistribution, angleCut):#m=mass of tungsten. eventNum is the number of events to be created (number of p1,p2,p3,p4,s,t,u,theta,phi multiplets)
        self.mSq=m**2                               #mass squared of the metal
        self.tungCharge=mCharge
        self.electronArgs=electronArgs
        self.tungArgs=tungArgs
        #self.tArgs=tArgs      #can be used for a more advanced proposal distributiont than the 1/t used.
        self.eLim=electronDistribution
        self.mLim=metalDistribution
        #self.tLim=tDistribution    #can be used for a more advanced proposal distributiont than the 1/t used.
        self.angleCut=angleCut
        self.distributeElecOrNot=True
        self.distributeTungOrNot=True
        if electronArgs[0] != 0:        #test whether correct arguments are passed to the gaussian 
            try:
                self.gaussian(1, *self.electronArgs)
            except:
                raise Exception("Wrong arguments (electronArgs) to gaussian!")
        else:
            self.distributeElecOrNot=False
        if tungArgs[0] != 0:            #test whether correct arguments are passed to the gaussian 
            try:
                self.gaussian(1, *self.tungArgs)
            except:
                raise Exception("Wrong arguments (tungArgs) to gaussian!")
        else:
            self.distributeTungOrNot=False
        """    
        try:     #can be used for a more advanced proposal distributiont than the 1/t used.
            self.f(1, *self.tArgs)
        except:
            raise Exception("Wrong arguments (tArgs) to f(x)!")
        """       
        self.eventNum=eventNum
        self.p3=[]                         #will later contain the outgoing momenta
        self.p4=[]
        self.weights=[]
        
    def clean(self):                       #used to reset the class variables for multiple runs of the generator
        self.p3=[]
        self.p4=[]
        self.weights=[]
        
    def addition(self, a, b):       #defines 4momentum addition
        return [a[0]+b[0],a[1]+b[1],a[2]+b[2],a[3]+b[3]]
    
    def scalarProduct(self, a, b):  #defines 4momentum scalar product
        return a[0]*b[0]-a[1]*b[1]-a[2]*b[2]-a[3]*b[3]
    
    def momentumNorm(self, a):      #defines absolute value of 3momentum
        return sqrt(a[1]**2+a[2]**2+a[3]**2)
    
    def kinematicFcn(self, a, b, c):
        return (a-b-c)**2-4*b*c
    
    def gaussian(self,x, sigma, mu):#gaussian, used for distributing initial momenta. Sigma=std. deviation, mu=mean (e.g. beam energy)
        return np.exp(-(x-mu)**2/(2*sigma**2))/sqrt(2*math.pi*sigma) #fix sigma
    
    def f(self,x):                  #used to distribute t according to 1/x
        return 1/x
        
    def inverse(self, limits, u):   #optional, used intead of "distrubutingLoop" to analytically give the 1/x distribution
        return limits[1]**u/(limits[0]**(u-1))
    
    def distributingLoop(self, fcn, N, limits, args):  #given any function "fcn", returns an array of variables distributed accoridingly
        stepsize=(limits[1]-limits[0])/N
        hist=np.arange(0,N)             #returns array 0 to N-1
        binstarts=[limits[0]+stepsize*x for x in hist]
        probs=np.array([0.01 for x in binstarts])   #"probabilities" of the different bins. Subsequent iterations of the loop below modify this accoriding to importance
        
        H1=[]                           #stores the distributed variables to be returned
        trials=0
        WrongOverestimate=0
        
        while len(H1) < self.eventNum: 
            trials+=1
            ibin=np.random.choice(hist,p=probs/sum(probs))  #select one of the bins at random, but taking into account their importance
            x=binstarts[ibin]+stepsize*np.random.random()   #pic a number in that bin
            if fcn(x, *args)>probs[ibin]:                   #if the estimation in probs was wrong, start over by clearing H1 and modifying "probs" accordingly
                WrongOverestimate+=1
                # Restart by clearing the lists:
                H1=[]
                probs[ibin]=fcn(x, *args)*1.1               #modify the probability for the wrongly estimated bin
                continue
            if np.random.random()<fcn(x, *args)/probs[ibin]:#save the number x if it is fits the ditribution
                H1+=[x]                                     #save the value
            else:
                continue
        print ("needed %s trials"%trials)
        print ("Had to restart: %s times"%WrongOverestimate)
        return H1
    
    #main method of the program. Facilitates the distribution of the initial momenta, of "t". 
    #Creates the phase-space point and computes the relevant quantities such as gamma and vCM.
    #Computes the matrix element and the jacobian. Computes the outgoing momenta. Finally calcultes the resulting integrals (eg the
    # cross-section). 
    def generateST(self):
        N=10
        if self.distributeElecOrNot:    #distribute the incoming electron momenta according to a function
            electronM=self.distributingLoop(self.gaussian, N, self.eLim, self.electronArgs)    #create the initial distibutions
            #plt.hist(electronM,normed=True,bins=200, histtype='step', color='r') #uncomment for visualisation of the distribution
            #plt.show()
        else:   #set the initial electron momenta to be all the same
            electronM=[self.electronArgs[1] for n in range(self.eventNum)]  #otherwise, all momenta equal the same value (nominal beam energy)
        if self.distributeTungOrNot:    #distribute the incoming electron momenta according to a function
            tungM=self.distributingLoop(self.gaussian, N, self.mLim, self.tungArgs)
            #plt.hist(tungM,normed=True,bins=200, histtype='step', color='g')  #uncomment for visualisation of the distribution
            #plt.show()
        else:   #set the initial tungsten momenta to be all the same
            tungM=[self.tungArgs[1] for n in range(self.eventNum)]
        t=[]
        thetaList=[]
        energyList=[]
        angleList=[]
        transverseMomentum=[]
        indexList=[]
        for j in range(self.eventNum):      #the loop that is the actual event generator. One iteration = one phase-space point     
            p1=[electronM[j],0,0,electronM[j]]                  #the incoming momenta in Lab frame
            p2=[sqrt(tungM[j]**2+self.mSq),0,0,tungM[j]]
            s=self.scalarProduct(self.addition(p1, p2),self.addition(p1, p2)) #the Mandelstam "s"
            sSq=sqrt(s)
            
            gamma=(p1[3]+sqrt(p2[3]**2+self.mSq))/sSq           #Lorentz factor
            vCM=abs(p1[3]+p2[3])/(p1[3]+sqrt(p2[3]**2+self.mSq))    #centre-of-momentum (CM) velocity
            
            p1[0]=sqrt(self.kinematicFcn(s,0,self.mSq))/(2*sSq) #boost to CM frame
            p1[3]=p1[0]
            p2[0]=(s+self.mSq)/(2*sSq)
            p2[3]=-p1[0]
            
            tMax=4*p1[0]**2                                 #max value of t -- that of backscattering. Depends on s 
            tMin=self.tMin(s, gamma, vCM)                   #converts the provided angleCut to tMin

            r=np.random.random()
            t.append(self.inverse([tMin, tMax], r))         #distribute t accoriding to 1/t (the proposal distribution for importance sampling)
            
            u=2*self.mSq-s+t[j]                             #the Mandelstam "u"
                             
            jacobian=abs(tMax-tMin)       #the jacobian
            #calculate the weights below for the cross-section integral that is importance sampled
            self.weights.append(t[j]*jacobian*self.matrixElement(s,t[j],u)/(16*math.pi*self.kinematicFcn(s, 0, self.mSq)))
            
            theta=math.acos(-2*s*t[j]/(self.kinematicFcn(s,0,self.mSq))+1)  #scattering angle
            thetaList.append(theta)
            angleList.append(math.atan2(abs(sin(theta)),(gamma*cos(theta)+gamma*vCM)))  #scattring angle in the Lab frame
            indexList.append(j+1)
            phi=np.random.random()*2*pi             #the trivial arimuthal angle
            self.generate34(j, p1[0], p2[0], theta, phi)          #calculates p3,p4 in CM frame
            self.boostBack(j, gamma, vCM, p1[0], p2[0], theta)    #boost back to Lab frame
            energyList.append(self.p3[j][0])                      #save outgoing electron energy for histogramming
            transverseMomentum.append(sqrt(self.p3[j][1]**2+self.p3[j][2]**2))
            #transverseMomentum.append(p1[0]*sin(theta))
        
        sumT=sum(t)
        """
        plt.title(r'Transverse Momentum')
        plt.xlabel(r'p$_{\mathrm{T}}$, [GeV]')
        plt.ylabel(r'Counts')
        plt.hist(transverseMomentum,bins=200, normed=False, histtype='step',weights=self.weights/sumT, color='b')
        savefig("transvMom_4GeV.png", dpi=600)
        plt.show()
        """
        """
        plt.title(r'Differential Cross-Section d$\sigma$/dE.')
        plt.xlabel(r'E, [GeV]')
        plt.ylabel(r'Weights [GeV$^{-2}$]')
        plt.hist(energyList,bins=200, normed=False, histtype='step', weights=self.weights/sumT, color='k')
        savefig("diffE{}GeV.png".format(self.electronArgs[1]), dpi=600)
        plt.show()
        
        plt.title(r'Differential Cross-Section d$\sigma/d|t|$.')
        plt.xlabel(r'$|t|$ [GeV$^2$]')
        plt.ylabel(r'Weights [GeV$^{-2}$]')
        plt.hist(t ,bins=200, normed=False, histtype='step', weights=self.weights/sumT, color='k')     #plot histograms
        savefig("diffT{}GeV.png".format(self.electronArgs[1]), dpi=600)
        plt.show()
        """
        crossSecPlainMC=self.regularInt()/(2.56*16*math.pi)     #use regular Monte Carlo (no importance sampling) to obtain the cross section
        crossSecImportS=sum(self.weights)/(sumT*2.56)           #use the results from the code above to obtain the cross section (importance sampling employed)
        print("Cross-section (regular M.C., no importance sampling, fixed initial state): ", crossSecPlainMC, " mb.")  #2.56 in denominator comes from converting GeV^2 to barns
        print("Cross-section (incoming momenta distributed, importance sampling employed): ", crossSecImportS, " mb.")
        #self.saveData(crossSecPlainMC,crossSecImportS) #uncomment to save cross-sections to file
        #return energyList, angleList, self.weights/(sumT*2.56)     #can be used to pass this data to outside the class for e.g. plotting
      
    def tMin(self, s, gamma, vCM):              #converts the angleCut to tMin
        square=1+vCM+1/(gamma**2*tan(self.angleCut)**2)
        l=self.kinematicFcn(s, 0, self.mSq)
        triangle=s/l+s/(l*gamma**2*tan(self.angleCut)**2)
        return abs(-.5*square/triangle+sqrt(.25*square**2/triangle**2-(l*(1+vCM**2+2*vCM))/(4*s*triangle)))
    
    def matrixElement(self, S, T, U):           #calculates the squared matrix element based on s,t,u
        return 8*(16*math.pi**2/137**2)*self.tungCharge**2*((S/2)**2+(U/2)**2+.5*self.mSq*(self.mSq-S-U-T))/(T**2) # Matrix element for spin - 1/2 particle
        #Below are alternative matrix elements for nuclei with other spins
        #return self.tungCharge**2*(16*math.pi**2/137**2)*(4*T*self.mSq+S**2-2*S*U-T**2+U**2) # Matrix element for spin - 0 particle
        #return self.tungCharge**2*(16*math.pi**2/137**2)*(self.mSq**2*(12*S**2+20*S*T+17*T**2)-2*T*self.mSq*(2*S**2+3*S*T+2*T**2)-4*self.mSq**3*(6*S+T)+12*self.mSq**4+S*T**2*(S+T))/(3*self.mSq**2)  # Matrix element for spin - 1 particle
    
    def generate34(self, i, E1, E2, theta, phi):  #calculate p3,p4 based on quntities generated in generatT
        self.p3.append([E1,E1*sin(theta)*cos(phi),E1*sin(theta)*sin(phi),E1*cos(theta)])
        self.p4.append([E2,E1*sin(pi-theta)*cos(pi+phi),E1*sin(pi-theta)*sin(pi+phi),E1*cos(pi-theta)])

    def boostBack(self, i, gamma, vCM, E1, E2, theta):         #boost p3,p4 back to LAB frame
        self.p3[i][0]=gamma*E1*(1+vCM*cos(theta))
        self.p3[i][3]=gamma*E1*(cos(theta)+vCM)
        self.p4[i][0]=gamma*(E2-E1*vCM*cos(theta))
        self.p4[i][3]=gamma*(vCM*E2-cos(theta)*E1)
           
    def regularInt(self):         #regular MC integral (no initially distributed momenta, no importance sampling)
        msq=[]
        tmpList=[]
        p1=[self.electronArgs[1],0,0,self.electronArgs[1]]                  #momenta corresponding to average of the initial distributions
        p2=[sqrt(self.tungArgs[1]**2+self.mSq),0,0,self.tungArgs[1]]
        s=self.scalarProduct(self.addition(p1, p2),self.addition(p1, p2))
        p1[0]=sqrt(self.kinematicFcn(s,0,self.mSq))/(2*sqrt(s))
        gamma=(p1[3]+sqrt(p2[3]**2+self.mSq))/sqrt(s)
        vCM=(p1[3]+p2[3])/(p1[3]+sqrt(p2[3]**2+self.mSq))
        tMin=self.tMin(s, gamma, vCM)
        for i in range (100000):
            tmp=tMin+(4*p1[0]**2-tMin)*np.random.random()
            tmpList.append(tmp)
            msq.append(self.matrixElement(s,tmp,2*self.mSq-s+tmp))
        """ 
        plt.hist(tmpList,bins=200, normed=False, histtype='step', weights=msq)  #uncomment to plot the histograms
        plt.title(r'Plain MC Integral')
        #plt.xlabel("t")
        plt.xlabel(r'$t, [GeV^2]$')
        #plt.ylabel("M^2")
        plt.ylabel(r'$\left|\overline{\mathcal{M}}\right|^2$')
        plt.show()
        """
        return (4*p1[0]**2-tMin)*sum(msq)/(100000*self.kinematicFcn(s, 0, self.mSq))
    def saveData(self, crossSecPlainMC, crossSecImportS):       #saves cross-sections to a .txt file
        with open("eventGenSavedData.txt", "a+") as f:
            f.write("Cross-section {} GeV (plainMC): {} mb.  ".format(self.electronArgs[1],crossSecPlainMC))
            f.write("Cross-section {} GeV (Importance S.): {} mb. \n".format(self.electronArgs[1],crossSecImportS))
        
# arguments to the function ditributing the initial electron momenta (sigma, mu). Set <sigma> to 0 for initial electron 
# momenta all having energy <mu>. Unit=GeV
electronArgs=[0, 4]      
electronDistribution=[3.95,4.05]    #min and max value of the above distribution in GeV
# arguments to the function ditributing the initial tungsten momenta (sigma, mu). Set <sigma> to 0 for initial tungsten 
# momenta all having energy <mu>. Unit=GeV
metalArgs=[0, 0]
metalDistribution=[-0.0003, 0.0003]    #min and max value of the above distribution in GeV
angleCut=math.radians(abs(20))  #the experimental cut on the outgoing angle in LAB frame. Must be positive.
#initiate: tungsten mass, tungsten charge, number of events to be created, and lists [electronArgs], [tungArgs] and the cut on the angle
events=eventGenerator(180, 74, 100000, electronArgs, metalArgs, electronDistribution, metalDistribution, angleCut)
events.generateST()
